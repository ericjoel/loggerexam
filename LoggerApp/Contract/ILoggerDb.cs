﻿namespace LoggerApp.Contract
{
    public interface ILoggerDb
    {
        void InitDatabase();
        void InsertLog(string message, string level, string type, string stackTrace, string innerException, string info);
    }
}