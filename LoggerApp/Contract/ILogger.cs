﻿using System;

namespace LoggerApp.Contract
{
    public interface ILogger
    {
        void Info(string message);
        void Warning(string message);
        void Error(string message, Exception exception);
    }
}
