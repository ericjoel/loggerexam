﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using LoggerApp.Contract;
using LoggerApp.Exceptions;

namespace LoggerApp.DatabaseImplementation
{
    public class LoggerDb : ILoggerDb
    {
        private readonly string connectionString;

        public LoggerDb()
        {
            connectionString = ConfigurationManager.ConnectionStrings["SqlDataConnectionString"].ConnectionString;
        }

        private void CreateTable()
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(@"  IF OBJECT_ID ( 'Logger', 'U' ) IS NOT NULL   
                                                                    DROP TABLE Logger;
                                                                ", connection))
                {
                    command.ExecuteNonQuery();
                }

                using (var command = new SqlCommand(@"CREATE TABLE Logger
                                                                (
	                                                                MessageValue VARCHAR(MAX),
	                                                                [Level] VARCHAR(20),
	                                                                [Type] VARCHAR(MAX),
	                                                                StackTrace VARCHAR(MAX),
	                                                                InnerException VARCHAR(MAX),
	                                                                AdditionalInfo VARCHAR(MAX),
                                                                    CreatedAt DATETIME
                                                                )", connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        private void CreateProcedure()
        {
            using (var connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (var command = new SqlCommand(@"  IF OBJECT_ID ( 'uspInsertLog', 'P' ) IS NOT NULL   
                                                                    DROP PROCEDURE uspInsertLog;  
                                                            ", connection))
                {
                    command.ExecuteNonQuery();
                }

                using (var command = new SqlCommand(@"  CREATE PROCEDURE uspInsertLog
                                                                (
	                                                                @MessageValue VARCHAR(MAX),
	                                                                @Level VARCHAR(20),
	                                                                @Type VARCHAR(MAX),
	                                                                @StackTrace VARCHAR(MAX),
	                                                                @InnerException VARCHAR(MAX),
	                                                                @AdditionalInfo VARCHAR(MAX)
                                                                )
                                                                AS
	                                                                INSERT INTO [dbo].Logger
	                                                                (
		                                                                MessageValue,
		                                                                [Level],
		                                                                [Type],
		                                                                StackTrace,
		                                                                InnerException,
		                                                                AdditionalInfo,
                                                                        CreatedAt
	                                                                )
	                                                                VALUES
	                                                                (
		                                                                @MessageValue,
		                                                                @Level,
		                                                                @Type,
		                                                                @StackTrace,
		                                                                @InnerException,
		                                                                @AdditionalInfo,
                                                                        SYSDATETIME()
	                                                                )
                                                            ", connection))
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public void InitDatabase()
        {
            CreateTable();
            CreateProcedure();
        }

        public void InsertLog(string message, string level, string type, string stackTrace, string innerException, string info)
        {
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (var command = new SqlCommand($"uspInsertLog", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@MessageValue", message);
                        command.Parameters.AddWithValue("@Level", level);
                        command.Parameters.AddWithValue("@Type", type ?? string.Empty);
                        command.Parameters.AddWithValue("@StackTrace", stackTrace ?? string.Empty);
                        command.Parameters.AddWithValue("@InnerException", innerException ?? string.Empty);
                        command.Parameters.AddWithValue("@AdditionalInfo", info ?? string.Empty);

                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (SqlException ex)
            {
                throw new LoggerDatabaseException("Error saving the log record", ex);
            }
        }
    }
}