﻿using System;
using LoggerApp.Contract;
using LoggerApp.DatabaseImplementation;
using LoggerApp.Factory;
using LoggerApp.Util;

namespace LoggerApp
{
    public class Program
    {
        public static ILogger Logger { get; set; }
        public static ILoggerDb LoggerDb { get; set; }

        public static void Main(string[] args)
        {
            LoggerDb = new LoggerDb();
            LoggerDb.InitDatabase();

            Logger = LoggerFactory.GetLogger(LoggerType.File);

            Logger.Info("Info Message");
            Logger.Warning("Warning Message");
            Logger.Error("Error Message", new Exception("Error Message"));


            Logger = LoggerFactory.GetLogger(LoggerType.Database);
            Logger.Info("Info Message");
            Logger.Warning("Warning Message");
            Logger.Error("Error Message", new Exception("Error Message"));

            Logger = LoggerFactory.GetLogger(LoggerType.Console);
            Logger.Info("Info Message");
            Logger.Warning("Warning Message");
            Logger.Error("Error Message", new Exception("Error Message"));

            Console.ReadLine();

        }
    }
}
