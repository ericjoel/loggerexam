﻿using LoggerApp.Contract;
using LoggerApp.DatabaseImplementation;
using LoggerApp.Exceptions;
using LoggerApp.Implementation;
using LoggerApp.Util;

namespace LoggerApp.Factory
{
    public static class LoggerFactory
    {
        public static ILogger GetLogger(LoggerType type, ILoggerDb loggerDb = null)
        {
            loggerDb = loggerDb ?? new LoggerDb();
            switch (type)
            {
                case LoggerType.Console:
                    return new LoggerConsole();
                case LoggerType.Database:
                    return new LoggerDatabase(loggerDb);
                case LoggerType.File:
                    return new LoggerFile();
                default:
                    throw new LoggerTypeException("Specified invalid type");
            }
        }
    }
}
