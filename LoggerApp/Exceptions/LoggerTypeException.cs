﻿using System;

namespace LoggerApp.Exceptions
{
    public class LoggerTypeException : ArgumentException
    {
        public LoggerTypeException(string message) : base(message)
        {
            
        }
    }
}
