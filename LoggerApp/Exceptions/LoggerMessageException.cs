﻿using System;

namespace LoggerApp.Exceptions
{
    public class LoggerMessageException : ArgumentException
    {
        public LoggerMessageException(string message) : base(message)
        {

        }
    }
}