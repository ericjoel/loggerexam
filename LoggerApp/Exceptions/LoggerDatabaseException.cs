﻿using System;
using System.Data.Common;

namespace LoggerApp.Exceptions
{
    public class LoggerDatabaseException : DbException
    {
        public LoggerDatabaseException(string message, Exception innerException) 
            : base(message, innerException)
        {
        }
    }
}
