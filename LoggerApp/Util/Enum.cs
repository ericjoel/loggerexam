﻿namespace LoggerApp.Util
{
    public enum Level : int
    {
        Info = 1,
        Warning = 2,
        Error = 3
    }

    public enum LoggerType : int
    {
        File = 1,
        Database = 2,
        Console = 3
    }
}
