﻿using System;

namespace LoggerApp.Util
{
    public static class LoggerUtilConsole
    {
        private static bool LogToConsoleError { get; set; }

        private static readonly object LockObject = new object();

        public static void Log(string message, Exception ex, Level level)
        {
            switch (level)
            {
                case Level.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case Level.Error:
                    LogToConsoleError = true;
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case Level.Info:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                default:
                    return;
            }

            if (LoggerUtilConsole.LogToConsoleError)
            {
                lock (LockObject)
                    Console.Error.WriteLine(message);
            }
            else
            {
                lock (LockObject)
                    Console.WriteLine(message);
            }

        }
    }
}
