﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Text;

namespace LoggerApp.Util
{
    public static class LoggerUtilFile
    {

        public static string LogFile
        {
            get
            {
                var directoryFile = ConfigurationManager.AppSettings["LogFileDirectory"];
                var directory = string.IsNullOrEmpty(directoryFile?.Trim()) ? null : directoryFile;
                var path = directory ?? $"{Environment.CurrentDirectory}\\Logger\\";
                return $"{path}LogFile_{DateTime.UtcNow:ddMMyy}.txt";
            }
        }

        private static readonly object LockObject = new object();



        private static void CreateDirectoryIfNotExists(string filename)
        {
            var directory = Path.GetDirectoryName(filename);
            if (string.IsNullOrEmpty(directory))
                throw new DirectoryNotFoundException("Invalid directory");
            Directory.CreateDirectory(directory);
        }

        public static void Write(Exception exception, Level level, string message)
        {

            if (string.IsNullOrEmpty(message))
                return;

            var stringBuilder = new StringBuilder(message.Length + 32);
            stringBuilder.Append(DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.ffff",
                CultureInfo.InvariantCulture));
            stringBuilder.Append(" ");

            stringBuilder.Append(Enum.GetName(typeof(Level), Convert.ToInt32(level)));
            stringBuilder.Append(" ");
            stringBuilder.Append(message);

            if (exception != null)
            {
                stringBuilder.Append(" Exception: ");
                stringBuilder.Append(exception);
            }

            var messageToSave = stringBuilder.ToString();

            lock (LockObject)
            {
                CreateDirectoryIfNotExists(LogFile);
                using (var streamWriter = File.AppendText(LogFile))
                {
                    streamWriter.WriteLine(messageToSave);
                    streamWriter.Close();
                }
            }

        }
    }
}
