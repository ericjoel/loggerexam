﻿using System;
using LoggerApp.Contract;
using LoggerApp.Exceptions;
using LoggerApp.Util;

namespace LoggerApp.Implementation
{
    public class LoggerConsole : ILogger
    {
        public void Info(string message)
        {
            if (string.IsNullOrEmpty(message?.Trim()))
                throw new LoggerMessageException("Message is empty");
            LoggerUtilConsole.Log(message, null, Level.Info);
        }

        public void Warning(string message)
        {
            if (string.IsNullOrEmpty(message?.Trim()))
                throw new LoggerMessageException("Message is empty");
            LoggerUtilConsole.Log(message, null, Level.Warning);
        }

        public void Error(string message, Exception exception)
        {
            if (string.IsNullOrEmpty((message??(exception?.Message))?.Trim()))
                throw new LoggerMessageException("Message is empty");
            LoggerUtilConsole.Log(message, exception, Level.Error);
        }
    }
}
