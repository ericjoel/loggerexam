﻿using System;
using LoggerApp.Contract;
using LoggerApp.Exceptions;
using LoggerApp.Util;

namespace LoggerApp.Implementation
{
    public class LoggerFile : ILogger
    {
        public void Info(string message)
        {
            if (string.IsNullOrEmpty(message?.Trim()))
                throw new LoggerMessageException("Message is empty");
            LoggerUtilFile.Write(null, Level.Info, message);
        }

        public void Warning(string message)
        {
            if (string.IsNullOrEmpty(message?.Trim()))
                throw new LoggerMessageException("Message is empty");
            LoggerUtilFile.Write(null, Level.Warning, message);
        }

        public void Error(string message, Exception exception)
        {
            if (string.IsNullOrEmpty((message ?? (exception?.Message))?.Trim()))
                throw new LoggerMessageException("Message is empty");
            LoggerUtilFile.Write(exception, Level.Error, message);
        }
    }
}
