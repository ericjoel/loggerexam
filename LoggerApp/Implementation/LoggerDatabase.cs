﻿using System;
using LoggerApp.Contract;
using LoggerApp.Exceptions;
using LoggerApp.Util;

namespace LoggerApp.Implementation
{
    public class LoggerDatabase : ILogger
    {
        private readonly ILoggerDb loggerDb;

        public LoggerDatabase(ILoggerDb loggerDb)
        {
            this.loggerDb = loggerDb;
        }

        public void Info(string message)
        {
            if (string.IsNullOrEmpty(message?.Trim()))
                throw new LoggerMessageException("Message is empty");
            WriteMessage(null, Level.Info, message);
        }

        public void Warning(string message)
        {
            if (string.IsNullOrEmpty(message?.Trim()))
                throw new LoggerMessageException("Message is empty");
            WriteMessage(null, Level.Warning, message);
        }

        public void Error(string message, Exception exception)
        {
            if (string.IsNullOrEmpty((message ?? (exception?.Message))?.Trim()))
                throw new LoggerMessageException("Message is empty");
            WriteMessage(exception, Level.Error, message);
        }

        private void WriteMessage(Exception exception, Level level, string message)
        {
            loggerDb.InsertLog(message??exception?.Message, Enum.GetName(typeof(Level), Convert.ToInt32(level)), exception?.GetType().Name, exception?.StackTrace, exception?.InnerException?.ToString(), exception?.Message??message);
        }
    }
}
