# Logger Exercise

Please review the following code snippet. Assume that all referenced assemblies have
been properly included.
The code is used to log different messages throughout an application. We want the
ability to be able to log to a text file, the console and/or the database. Messages can be
marked as message, warning or error. We also want the ability to selectively be able to
choose what gets logged, such as to be able to log only errors or only errors and
warnings.

- If you were to review the following code, what feedback would you give? Please
be specific and indicate any errors that would occur as well as other best practices
and code refactoring that should be done.
- Rewrite the code based on the feedback you provided in question 1. Please
include unit tests on your code.

# Code

```
using System;
using System.Linq;
using System.Text;
public class JobLogger {
	private static bool _logToFile;
	private static bool _logToConsole;
	private static bool _logMessage;
	private static bool _logWarning;
	private static bool _logError;
	private static bool LogToDatabase;
	private bool _initialized;
	public JobLogger(bool logToFile, bool logToConsole, bool logToDatabase, bool logMessage, bool logWarning, bool logError) {
		_logError = logError;
		_logMessage = logMessage;
		_logWarning = logWarning;
		LogToDatabase = logToDatabase;
		_logToFile = logToFile;
		_logToConsole = logToConsole;
	}
	public static void LogMessage(string message, bool message, bool warning, bool error) {
		message.Trim();
		if (message == null || message.Length == 0) {
			return;
		}
		if (!_logToConsole && !_logToFile && !LogToDatabase) {
			throw new Exception("Invalid configuration");
		}
		if ((!_logError && !_logMessage && !_logWarning) || (!message && !warning && !error)) {
			throw new Exception("Error or Warning or Message must be specified");
		}
		System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(System.Configuration.ConfigurationManager.AppS ettings["ConnectionString"]);
		connection.Open();
		int t;
		if (message && _logMessage) {
			t = 1;
		}
		if (error && _logError) {
			t = 2;
		}
		if (warning && _logWarning) {
			t = 3;
		}
		System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand("Insert into Log Values('" + message + "', " + t.ToString() + ")");
		command.ExecuteNonQuery();
		string l;
		if (!System.IO.File.Exists(System.Configuration.ConfigurationManager.AppSettings["Log FileDirectory"] + "LogFile" + DateTime.Now.ToShortDateString() + ".txt")) {
			l = System.IO.File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings[" LogFileDirectory"] + "LogFile" + DateTime.Now.ToShortDateString() + ".txt");
		}
		if (error && _logError) {
			l = l + DateTime.Now.ToShortDateString() + message;
		}
		if (warning && _logWarning) {
			l = l + DateTime.Now.ToShortDateString() + message;
		}
		if (message && _logMessage) {
			l = l + DateTime.Now.ToShortDateString() + message;
		}
		System.IO.File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["LogFileDirectory"] + "LogFile" + DateTime.Now.ToShortDateString() + ".txt", l);
		if (error && _logError) {
			Console.ForegroundColor = ConsoleColor.Red;
		}
		if (warning && _logWarning) {
			Console.ForegroundColor = ConsoleColor.Yellow;
		}
		if (message && _logMessage) {
			Console.ForegroundColor = ConsoleColor.White;
		}
		Console.WriteLine(DateTime.Now.ToShortDateString() + message);
	}
}
```

# Code Review

- Unreadable code.
- Complex code.
- Poorly descriptive variables.
- Poorly maintainable code.
- Violation of SOLID principles.
- No error handling

# Generals Errors

- Code conventios is not constant on private variables.
- Message variable on LogMessage method is the same as the attribute, should be more descriptive.
- Trim method returns a string is not assigned to a variable.
- To check if a string is null or empty, a function already exists inside string class (string.IsNullOrEmpty).
- LogMessage method take arguments that already exists on class as attributes.
- Database, File and Console are not used on method to condition where to log the message.
- Sql code should using to close the connection and command.
- Use procedures to avoid sql injection problem.
- Command doesn't reference the connection.
- Log Table doesn't save enough information.
- File handling is poor
- Needs to lock the file writer to avoid problems like try to open file that is already open.
- Console.Error should be called, just in case of error.


# Feedback

- Create classes that have responsibility over a single part of the functionality.
- Create custom exceptions to handle the errors.
- Create variables that describes their real functionality
- Follow SOLID principles to create a maintainable code.
- More interfaces to avoid coupling between classes.

# Tests Results

![alt text](CoverTests/CoverTests.png "Cover Test")

# Install
- If you want to specified a route to save the log file, you should modify LogFileDirectory Key on App.conf
    - Example: C:\Users\USER\Documents\LoggerReal\

- Database
    - On App.conf you should set your default connection on SqlDataConnectionString at connectionStrings section.

- Tests:
    - On test project the database is mocked to avoid connection problems.