﻿using System;
using LoggerApp.Contract;
using LoggerApp.Exceptions;
using LoggerApp.Factory;
using LoggerApp.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace LoggerApp.Tests
{
    [TestClass]
    public class LoggerDatabaseTest
    {
        private ILoggerDb loggerDb;

        [TestInitialize]
        public void TestInitialize()
        {
            var mockLoggerDb = new Mock<ILoggerDb>();
            var message = string.Empty;
            var level = string.Empty;
            var stacktrace = string.Empty;
            var innerException = string.Empty;
            var type = string.Empty;
            var info = string.Empty;

            mockLoggerDb.Setup(sp => sp.InsertLog(message, level, type, stacktrace, innerException, info));

            loggerDb = mockLoggerDb.Object;
        }

        [TestMethod]
        [ExpectedException(typeof(LoggerMessageException))]
        public void TestInfoLoggerMessageException()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.Database);
            logger.Info(null);
        }

        [TestMethod]
        [ExpectedException(typeof(LoggerMessageException))]
        public void TestWarningLoggerMessageException()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.Database);
            logger.Warning(null);
        }

        [TestMethod]
        [ExpectedException(typeof(LoggerMessageException))]
        public void TestErrorLoggerMessageException()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.Database);
            logger.Error(null, null);
        }

        [TestMethod]
        public void TestLoggerDatabaseType()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.Database, loggerDb);
            Assert.IsTrue(logger is Implementation.LoggerDatabase);
        }

        [TestMethod]
        public void TestLoggerInfoMessage()
        {
            try
            {
                var logger = LoggerFactory.GetLogger(LoggerType.Database, loggerDb);
                logger.Info("Test Message Info - Database");
                Assert.IsTrue(true);
            }
            catch (Exception)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void TestLoggerWarningMessage()
        {
            try
            {
                var logger = LoggerFactory.GetLogger(LoggerType.Database, loggerDb);
                logger.Warning("Test Message Warning - Database");
                Assert.IsTrue(true);
            }
            catch (Exception)
            {
                Assert.IsTrue(false);
            }
        }

        [TestMethod]
        public void TestLoggerErrorMessage()
        {
            try
            {
                var logger = LoggerFactory.GetLogger(LoggerType.Database, loggerDb);
                logger.Error("Test Message Error - Database", new Exception());
                Assert.IsTrue(true);
            }
            catch (Exception)
            {
                Assert.IsTrue(false);
            }
        }
    }
}
