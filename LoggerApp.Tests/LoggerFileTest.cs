﻿using LoggerApp.Exceptions;
using LoggerApp.Factory;
using LoggerApp.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LoggerApp.Tests
{
    [TestClass]
    public class LoggerFileTest
    {
        [TestInitialize]
        public void TestInitialize()
        {

        }

        [TestMethod]
        [ExpectedException(typeof(LoggerMessageException))]
        public void TestInfoLoggerMessageException()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.File);
            logger.Info(null);
        }

        [TestMethod]
        [ExpectedException(typeof(LoggerMessageException))]
        public void TestWarningLoggerMessageException()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.File);
            logger.Warning(null);
        }

        [TestMethod]
        [ExpectedException(typeof(LoggerMessageException))]
        public void TestErrorLoggerMessageException()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.File);
            logger.Error(null, null);
        }

        [TestMethod]
        public void TestLoggerFileType()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.File);
            Assert.IsTrue(logger is Implementation.LoggerFile);
        }

        [TestMethod]
        public void TestLoggerInfoMessage()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.File);
            logger.Info("Test MessageInfo - File");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void TestLoggerWarningMessage()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.File);
            logger.Warning("Test MessageWarning - File");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void TestLoggerErrorMessage()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.File);
            logger.Error("Test MessageError - File", new System.Exception("Error"));
            Assert.IsTrue(true);
        }

    }
}
