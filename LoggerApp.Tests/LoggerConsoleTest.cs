﻿using System;
using LoggerApp.Exceptions;
using LoggerApp.Factory;
using LoggerApp.Util;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LoggerApp.Tests
{
    [TestClass]
    public class LoggerConsoleTest
    {
        [TestMethod]
        public void TestLoggerConsoleType()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.Console);
            Assert.IsTrue(logger is Implementation.LoggerConsole);
        }

        [TestMethod]
        [ExpectedException(typeof(LoggerMessageException))]
        public void TestInfoLoggerMessageException()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.Console);
            logger.Info(null);
        }

        [TestMethod]
        [ExpectedException(typeof(LoggerMessageException))]
        public void TestWarningLoggerMessageException()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.Console);
            logger.Warning(null);
        }

        [TestMethod]
        [ExpectedException(typeof(LoggerMessageException))]
        public void TestErrorLoggerMessageException()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.Console);
            logger.Error(null, null);
        }

        [TestMethod]
        public void TestLoggerInfoMessage()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.Console);
            logger.Info("TestMessageInfo - Console");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void TestLoggerWarningMessage()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.Console);
            logger.Warning("TestMessageWarning - Console");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void TestLoggerErrorMessage()
        {
            var logger = LoggerFactory.GetLogger(LoggerType.Console);
            logger.Error("TestMessageError - Console", new Exception("Console Error"));
            Assert.IsTrue(true);
        }
    }
}
